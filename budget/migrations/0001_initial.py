# Generated by Django 4.1.7 on 2023-03-09 16:11

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bucket',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('total', models.FloatField(blank=True, default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('bucket_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='budget.bucket')),
                ('limit', models.FloatField()),
            ],
            bases=('budget.bucket',),
        ),
        migrations.CreateModel(
            name='Income',
            fields=[
                ('bucket_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='budget.bucket')),
                ('plan', models.IntegerField()),
            ],
            bases=('budget.bucket',),
        ),
        migrations.CreateModel(
            name='Wallet',
            fields=[
                ('bucket_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='budget.bucket')),
            ],
            bases=('budget.bucket',),
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('amount', models.FloatField()),
                ('comment', models.TextField(blank=True)),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='source_transaction', to='budget.bucket')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='target', to='budget.bucket')),
            ],
        ),
    ]
