from django import forms
from .models import Wallet, Expense

class ExpenseForm(forms.ModelForm):
    title = forms.CharField(
        max_length=255,
        label='Expense title',
        widget=forms.widgets.TextInput(
            attrs={
                'class': 'form-control',
                'type': 'text'
            }
        )
        )
    class Meta:
        model = Expense
        fields = ['title','total','limit']
    
class WalletForm(forms.ModelForm):
    title = forms.CharField(
        max_length=255,
        label='Wallet title',
        widget=forms.widgets.TextInput(
            attrs={
                'class': 'form-control',
                'type': 'text'
            }
        )
        )
    total = forms.FloatField(
        label='Current amount',
        disabled=True,
        widget=forms.widgets.TextInput(
            attrs={
                'class': 'form-control',
                'type': 'text',
                # 'disabled': None
            }
        )
        )
    
    class Meta:
        model = Wallet
        exclude = ()