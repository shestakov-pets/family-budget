import json
from calendar import monthrange
from django.contrib import messages
from django.db import DatabaseError, transaction as db_transaction
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from itertools import chain
from .models import Income, Wallet, Expense, Transaction
from .forms import WalletForm, ExpenseForm
from .templatetags.color import card_collor

def get_card_collor(total, limit):
    tz = timezone.now()
    days_in_month = monthrange(tz.year, tz.day)[1]
    day_expense = limit / days_in_month
    if total <= limit:
        if day_expense * tz.day >= total:
            return '#198754'
        else:
            return '#fd7e14'
    else:
        return '#dc3545'

def prepare_slides(objects: list, num_on_slide) -> list:
    output_objects = []
    slide = 1
    for x in range(0,len(objects),num_on_slide):
        tmp_objects = [objects[y] for y in range(x,x+num_on_slide) if y < len(objects)]
        output_objects.append(tmp_objects)
        slide += 1
    return output_objects

def index(request):
    all_incomes = Income.objects.all()
    incomes = prepare_slides(all_incomes, 4)
    all_wallets = Wallet.objects.all()
    wallets = prepare_slides(all_wallets, 4)
    expenses = prepare_slides(Expense.objects.all(), 4)
    context = {
        'incomes_list': incomes,
        'wallet_list': wallets,
        'expenses_list': expenses
    }
    return render(request, 'budget/index.html', context)

def get_wallet(request, wallet_id):
    wallet = get_object_or_404(Wallet, pk=wallet_id)
    transactions = Transaction.objects.filter(Q(source=wallet.id) | Q(target=wallet.id)).order_by('-date')
    context = {
        'form': WalletForm(instance=wallet),
        'wallet': wallet,
        'transactions': transactions
    }
    return render(request, 'budget/wallet.html', context)

def update_wallet(request, wallet_id):
    wallet = get_object_or_404(Wallet, pk=wallet_id)
    if request.method == 'POST':
        form = WalletForm(request.POST, instance=wallet)
        if form.is_valid():
            form.save()
            return redirect('index')
    return redirect('index')

def get_expense(request, expense_id):
    """Render expense page"""
    context = {
        'expense_id': expense_id
    }
    return render(request, 'budget/expense.html', context)

def get_expense_detail(request, expense_id):
    """Get expense detail by id"""
    expense = get_object_or_404(Expense, pk=expense_id)
    collor = card_collor(expense)
    context = {
        'expense': expense,
        'card_collor': collor
    }
    return render(request, 'budget/expense_detail.html', context)

def get_expense_transactions(request, expense_id):
    """Get all expense transaction by expense id"""
    transactions = Transaction.objects.filter(target=expense_id).order_by('-date')
    context = {
        'transactions': transactions
    }
    return render(request, 'budget/expense_table.html', context)

def edit_expense(request, expense_id):
    """Update expense by id"""
    expense = get_object_or_404(Expense, pk=expense_id)
    if request.method == 'POST':
        form = ExpenseForm(request.POST, instance=expense)
        if form.is_valid():
            form.save()
            return HttpResponse(
                status=204,
                headers={
                    'HX-Trigger': json.dumps({
                        'expenseChange': None,
                    })
                })
    form = ExpenseForm(instance=expense)
    context = {
        'form': form,
        'expense': expense
    }
    return render(request, 'budget/expense_form.html', context)

def create_transaction(request, target_id):
    to_wallet = False
    try:
        # Check if target is Wallet
        target = Wallet.objects.get(pk=target_id)
        incomes = Income.objects.order_by('title')
        wallets = Wallet.objects.order_by('title')
        sources = list(chain(incomes,wallets))
        to_wallet = True
    except Wallet.DoesNotExist:
        # Check if target is Expense
        target = get_object_or_404(Expense, pk=target_id)
        sources = Wallet.objects.order_by('title')
    content = {
        'sources': sources,
        'target': target,
        'to_wallet': to_wallet
    }
    return render(request, 'budget/transaction_add.html', content)

def complete_transaction(request, target_id):
    from_income = False
    if request.method == 'POST':
        try:
            # If target wallet
            target = Wallet.objects.get(pk=target_id)
            try:
                # Check if transaction is between wallets
                source = Wallet.objects.get(pk=request.POST.get('sourceSelect'))
            except Wallet.DoesNotExist:
                # Check if source is Income
                source = get_object_or_404(Income, pk=request.POST.get('sourceSelect'))
                from_income = True
        except Wallet.DoesNotExist:
            # If target is Expense and source is Wallet
            target = get_object_or_404(Expense, pk=target_id)
            source = Wallet.objects.get(pk=request.POST.get('sourceSelect'))
        
        # Get transaction's amount
        try:
            amount = float(request.POST.get('transactionAmount'))
        except ValueError:
            messages.warning(request, 'Enter correct amount')
            return redirect('create_transaction', wallet_id=target.id)
        if amount == 0:
            messages.warning(request, 'Enter correct amount')
            return redirect('create_transaction', wallet_id=target.id)
        comment = request.POST.get('transactionComment')
        target.total += amount
        # Create money transaction
        transaction = Transaction(
            source=source,
            target=target,
            amount=amount,
            comment=comment
            )
        if from_income:
            # Source is Income
            source.total += amount
        else:
            # Source is wallet
            # Check that source wallet has enough amount
            if source.total - amount < 0:
                messages.warning(request, f'Insufficient funds in the "{ source.title }"')
                return redirect('create_transaction_wallet', wallet_id=target.id)
            source.total -=amount
        try:
            # Complete transaction
            with db_transaction.atomic():
                transaction.save()
                target.save()
                source.save()
        except DatabaseError:
            messages.warning(request, 'Could not complete transaction')
    return redirect('index')
