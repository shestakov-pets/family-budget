from calendar import monthrange
from django.utils import timezone
from django import template


register = template.Library()

@register.simple_tag
def card_collor(bucket):
    tz = timezone.now()
    days_in_month = monthrange(tz.year, tz.day)[1]
    day_expense = bucket.limit / days_in_month
    if bucket.total <= bucket.limit:
        if day_expense * tz.day >= bucket.total:
            return '#198754'
        else:
            return '#fd7e14'
    else:
        return '#dc3545'