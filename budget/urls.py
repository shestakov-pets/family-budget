from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    path('wallet/<int:wallet_id>/', views.get_wallet, name='get_wallet'),
    path('wallet/update/<int:wallet_id>/', views.update_wallet, name='update_wallet'),

    path('expense/<int:expense_id>/', views.get_expense, name='get_expense'),
    path('expense/<int:expense_id>/detail', views.get_expense_detail, name='get_expense_detail'),
    path('expense/<int:expense_id>/transactions', views.get_expense_transactions, name="expense_transactions"),
    path('expense/<int:expense_id>/edit', views.edit_expense, name='edit_expense'),

    path('transaction/new/to/<int:target_id>/', views.create_transaction, name='create_transaction'),
    path('transaction/complete/to/<int:target_id>/', views.complete_transaction, name='complete_transaction'),

]
