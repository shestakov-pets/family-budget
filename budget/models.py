from django.db import models
from django.utils import timezone

class Bucket(models.Model):
    title = models.CharField(max_length=255)
    total = models.FloatField(
        blank=True,
        default=0
    )
    def __str__(self) -> str:
        return self.title

class Income(Bucket):
    plan = models.IntegerField()

class Wallet(Bucket):
    pass

class Expense(Bucket):
    limit = models.FloatField()


class Transaction(models.Model):
    date = models.DateTimeField(
        default=timezone.now,
        null=False,
        blank=False
    )
    source = models.ForeignKey(
        Bucket,
        related_name='source_transaction',
        on_delete=models.PROTECT
    )
    target = models.ForeignKey(
        Bucket,
        related_name='target',
        on_delete=models.PROTECT
    )
    amount = models.FloatField(
        blank=False,
        null=False
    )
    comment = models.TextField(
        blank=True
    )

    def __str__(self) -> str:
        return f"{self.source} -> {self.target}"