from django.shortcuts import render
from .forms import UserCreationForm

def register(request):
    context = {}
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            # Redirect to login here
            
        context['form'] = form
    else: 
        form = UserCreationForm()
        context['form'] = form
    return render(request, 'users/register.html', context)