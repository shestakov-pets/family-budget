;(function(){
    var expenseModal = new bootstrap.Modal(document.getElementById('expenseModal'))
  
    htmx.on('htmx:afterSwap', (e) => {
      if (e.detail.target.id === 'expenseModalForm') {
        expenseModal.show()
      } 
    })
  
    htmx.on('htmx:beforeSwap', (e) => {
      console.log(e.detail)
      if (e.detail.target.id === 'expenseModalForm' && !e.detail.xhr.response) {
        expenseModal.hide()
        e.detail.shouldSwap = false
      }
    })
  
    htmx.on('hidden.bs.modal', (e) => {
      document.getElementById('expenseModalForm').innerHTML = ''
    })
  })()